package dev.rehm.other;

import dev.rehm.geometry.Circle;

public class OtherApp {

    public static void main(String[] args) {
        // we can use classes outside of the package that they are declared, but they must be imported
        Circle c = new Circle();

        // we can also use the fully qualified class name instead of importing
        dev.rehm.geometry.Rectangle r = new dev.rehm.geometry.Rectangle();

    }
}
