package dev.rehm.other;

public class StringDriver {

    public static void main(String[] args) {
        /*
        String str1 = "hello world"; // this is a string literal
        char c = str1.charAt(3);
        System.out.println("character at position 3: "+ c);

        String subStr = str1.substring(5);
        System.out.println("substring: "+subStr);

        System.out.println("trimmed substring: "+ subStr.trim());

        int l = str1.length();
        int x = 12;
        System.out.println("original string length: "+l);
         */
        StringBuilder builder = new StringBuilder("Hello World");
        StringBuilder builder2 = builder.append("!!!");
        System.out.println(builder);
        builder.insert(4, "-");
        System.out.println(builder);
        builder.delete(6,8);
        System.out.println(builder);

    }
}
