package dev.rehm.geometry;

public abstract class Shape implements Computable {
    // abstract classes allow us to define instance variables
    private String color;

    Shape(){
//        super();//calling to the object class' no argument constructor
    }

    // while this constructor cannot be used to instantiate a shape directly,
        // this initialization logic can be used in subclasses
    Shape(String colorParam){
//        super();
        this.color = colorParam;
    }

    // by default, declared methods are concrete (fully implemented with a method body)
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public abstract void draw();

}
