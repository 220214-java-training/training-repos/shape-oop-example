package dev.rehm.geometry;

public interface Computable {
    // we want to be able to compute certain values

    public double findArea();
    public double findPerimeter();


}
