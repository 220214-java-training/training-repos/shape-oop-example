package dev.rehm.geometry;

public class Rectangle extends Shape{

    public static final int SIDES = 4;
    private double height;
    private double width;

    public Rectangle(){
//        super();
    }

    Rectangle(String color, double height, double width){
        super(color);
        this.height = height;
        this.width = width;
    }

    public double getHeight(){
        return height;
    }

    public void setHeight(double height){
        this.height = height;
    }

    public double getWidth(){
        return width;
    }

    public void setWidth(double width){
        this.width = width;
    }

    @Override
    public void draw() {
        System.out.println("Drawing rectangle");
    }

    @Override
    public String toString(){
        return "Rectangle: color = "+this.getColor()+", width = "+width+", height = "+height;
    }

    @Override
    public double findArea() {
        return width*height;
    }

    @Override
    public double findPerimeter() {
        return 2*(width+height);
    }
}
