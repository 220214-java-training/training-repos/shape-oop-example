package dev.rehm.geometry;


public class Circle extends Shape {

    // instance variables are provided default values (0, 0.0, false, null)
    private double radius;

    // implicitly provides a no argument constructor
    public Circle(){
        super();
    }

    Circle(String circleColorParam){
//        super();
//        this.color = circleColorParam;
        super(circleColorParam);
    }

    /*
        Constructor overloading is an example of polymorphism (specifically compile time polymorphism)
     */
    Circle(String circleColorParam, double radiusParam){
        super(circleColorParam);
        this.radius = radiusParam;
    }

    public double getRadius(){
        return this.radius;
    }

    public void setRadius(double radius) {
        int upperLimit = 1000;
        if(radius>=0){
            this.radius = radius;
        }
    }

    @Override
    public void draw() {
        System.out.println("Drawing a circle");
    }

    /*
        Another example of polymorphism, method overriding (runtime polymorphism)
     */
    @Override
    public String toString(){
        return "Circle: color = "+ this.getColor() + ", radius = "+this.radius;
    }

    /*
    the equals method defines value equality
     */
    @Override
    public boolean equals(Object obj){
        // compare two different objects
        // compare the two radius values and see if they're the same
        // accessing the instance variable
        // assigning to currentRadius local variable
        double currentRadius = this.radius;
        // make sure the other object is a circle
        if(obj.getClass() == this.getClass()){ // check the class for the instance and the param
            Circle otherCircle = (Circle) obj;
            double otherRadius = otherCircle.radius;
            if(currentRadius == otherRadius && this.getColor().equals(otherCircle.getColor())){
                return true;
            }
        }
        return false;
    }


    @Override
    public double findArea() {
        return Math.PI*radius*radius;
    }

    @Override
    public double findPerimeter() {
        return 2*radius*Math.PI;
    }
}
