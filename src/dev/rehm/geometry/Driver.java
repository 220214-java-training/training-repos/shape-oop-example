package dev.rehm.geometry;
/*
    - when we define classes within a package, we have a package declaration as the first line in our class file
    - packages help us organize our code, and also allow us to distinguish between multiple classes of the same
        name
    -
 */

public class Driver {

    public static void main(String[] args){
        /*
//        Shape s = new Shape(); //we cannot instantiate Shape if it's an abstract class
        Circle c = new Circle();
        System.out.println(c.color); // Strings begin with default type of null
        c.color = "blue";
        System.out.println(c.color);

        // we attempt to change the radius to an invalid value
        c.setRadius(-3000);
//        c.radius = -3000;
        // we see the radius was not changed successfully
        System.out.println(c.getRadius());

        // attempt to set the radius to a valid value
        c.setRadius(34);
        System.out.println(c.toString());
//        Circle c2 = new Circle("green");
//        Circle c3 = new Circle(29);
        /*
            Another example of polymorphism - covariance
         */
        /*
        Shape s = new Circle();
        Object o = new Circle();
//        Circle c4 = new Object(); // we cannot do this


        Rectangle r1 = new Rectangle();
        r1.setHeight(3);
        r1.setWidth(4);
        System.out.println(r1); // automatically calls the .toString method

        Rectangle r2 = new Rectangle("red", 1,2);
        System.out.println(r2); // automatically calls the .toString method
        */

        Rectangle r1 = new Rectangle("blue",4,5);
        Rectangle r2 = new Rectangle("blue", 4, 5);
        Rectangle r3 = r1;
//        Rectangle.SIDES = 8;
//        System.out.println(Rectangle.SIDES);
//        r1.color = "green";


        // Using a constructor to instantiate a new object
        Object o = new Object();  // not an abstract class because I can create an instance

        /*
        System.out.println("r1==r3?");
        System.out.println(r1==r3); // reference equality
        System.out.println("r1==r2?");
        System.out.println(r1==r2); // reference equality
        System.out.println("r1.equals(r2)?");
        System.out.println(r1.equals(r2)); // we want to override the equals method to have a meaningful
        // implementation for the rectangle class - this is inherited from the object class which has no
        // knowledge of the rectangle class or its fields
        */

        Circle c = new Circle("blue",3);
        boolean result1 = c.equals(new Circle("green",8));
        boolean result2 = c.equals("some random String");
        boolean result3 = c.equals(new Rectangle());
        boolean result4 = c.equals(new Circle("blue", 3));

        System.out.println(result1);
        System.out.println(result2);
        System.out.println(result3);
        System.out.println(result4);


    }

}
